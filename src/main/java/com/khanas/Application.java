package com.khanas;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class Application {

    static Logger logger = LogManager.getLogger();
    static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
        while(true) {
            logger.info("1. View with enum");
            logger.info("1. View with map");
            logger.info("3. Exit");
            logger.info("Enter: ");
            int index = sc.nextInt();
            if(index==1) {
                new EnumView();
            }else if(index==2){
                new MapView();
            }else if(index==3){
                break;
            }else {
                logger.error("Wrong index");
            }
        }
    }
}
