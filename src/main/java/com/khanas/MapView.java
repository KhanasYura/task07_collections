package com.khanas;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MapView {
    Logger logger = LogManager.getLogger();
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner sc = new Scanner(System.in);
    BinaryTreeMap<Integer, String> tree;
    public MapView() {
        tree = new BinaryTreeMap<Integer, String>(new Comparator<Integer>() {
            public int compare(Integer o1, Integer o2) {
                return o1.compareTo(o2);
            }
        });
        menu = new LinkedHashMap<String, String>();
        menu.put("1", "  1 - add to tree");
        menu.put("2", "  2 - get from tree");
        menu.put("3", "  3 - remove from tree");
        menu.put("4", "  4 - print tree");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<String, Printable>();
        methodsMenu.put("1", this::add);
        methodsMenu.put("2", this::get);
        methodsMenu.put("3", this::remove);
        methodsMenu.put("4", this::print);

        show();
    }

    public void add(){
        logger.info("Enter index of binary tree: ");
        int index = sc.nextInt();
        logger.info("Enter string for binary tree: ");
        sc.nextLine();
        String str = sc.nextLine();
        tree.put(index,str);
    }

    public void get(){
        logger.info("Enter index of binary tree: ");
        int index = sc.nextInt();
        sc.nextLine();
        logger.info(tree.get(index));
    }

    public void remove(){
        logger.info("Enter index of binary tree: ");
        int index = sc.nextInt();
        sc.nextLine();
        tree.remove(index);
    }
    public void print(){
        Map<Integer,String> arr = tree.printTree();
        logger.info(arr);
    }

    private void outputMenu() {
        logger.info("MENU:");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.info("Please, select menu point.");
            keyMenu = sc.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
