package com.khanas;

import java.util.*;

public class BinaryTreeMap<K, V> {

    static class Node<K, V> implements Map.Entry<K, V> {
        private K key;
        private V value;
        private Node<K, V> parent;
        private Node<K, V> left;
        private Node<K, V> right;

        public K getKey() {
            return key;
        }

        public void setKey(K key) {
            this.key = key;
        }

        public V getValue() {
            return value;
        }

        public V setValue(V value) {
            V old = this.value;
            this.value = value;
            return old;
        }

        public Node<K, V> getParent() {
            return parent;
        }

        public void setParent(Node<K, V> parent) {
            this.parent = parent;
        }

        public Node<K, V> getLeft() {
            return left;
        }

        public void setLeft(Node<K, V> left) {
            this.left = left;
        }

        public Node<K, V> getRight() {
            return right;
        }

        public void setRight(Node<K, V> right) {
            this.right = right;
        }

        public boolean hasNoChildren() {
            return left == null && right == null;
        }

        @Override
        public String toString() {
            return "Node: key= " + key + ", value= " + value;
        }
    }

    private Node<K, V> root;
    private Comparator<K> comparator;
    private int size;

    public BinaryTreeMap(Comparator<K> comparator) {
        this.comparator = comparator;
    }

    public void put(K key, V value) {
        if (root == null) {
            root = entry(key, value);
            size++;
        } else {
            Node<K, V> e = findPlaceToInsert(key, root);
            if (eq(e.key, key)) {
                V old = e.value;
                e.value = value;
            } else {
                Node<K, V> newEntry = entry(key, value);
                newEntry.parent = e;
                if (desc(key, e.key)) {
                    e.left = newEntry;
                } else {
                    e.right = newEntry;
                }
                size++;
            }
        }
    }

    public V get(Object key) {
        Node<K, V> e = getEntry((K) key, root);
        return e == null ? null : e.value;
    }

    public void clear() {
        root = null;
        size = 0;
    }

    public int size() {
        return size;
    }

    public boolean containsKey(Object key) {
        return getEntry((K) key, root) != null;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public V remove(Object key) {
        Node<K, V> removed = getEntry((K) key, root);
        if (removed == null) {
            return null;
        } else {
            Node<K, V> parent = removed.parent;
            if (removed.hasNoChildren()) {
                if (removed == root) {
                    root = null;
                    size--;
                    return removed.value;
                }
                if (comparator.compare(removed.key, parent.key) < 0) {
                    parent.left = null;
                } else {
                    parent.right = null;
                }
            }else{
                Map<K,V> arrLeft = new HashMap<>();
                Map<K,V> arrRight = new HashMap<>();
                if(removed.getLeft()!=null){
                    runTree(removed.getLeft(), arrLeft);
                }
                if(removed.getRight()!=null){
                    runTree(removed.getRight(), arrRight);
                }
                if(parent.getRight().getKey()==removed.getKey())
                    parent.right = null;
                else{
                    parent.left = null;
                }
                for (Map.Entry<K, V> entry : arrRight.entrySet()) {
                    put(entry.getKey(),entry.getValue());
                }
                for (Map.Entry<K, V> entry : arrLeft.entrySet()) {
                    put(entry.getKey(),entry.getValue());
                }

            }
            size--;
            return removed.value;
        }
    }

    private Node<K, V> findPlaceToInsert(K key, Node<K, V> curr) {
        if (eq(key, curr.key)) {
            return curr;
        } else if (desc(key, curr.key) && curr.left != null) {
            return findPlaceToInsert(key, curr.left);
        } else if (absc(key, curr.key) && curr.right != null) {
            return findPlaceToInsert(key, curr.right);
        } else {
            return curr;
        }
    }

    private Node<K, V> getEntry(K key, Node<K, V> curr) {
        while (curr != null) {
            int c = comparator.compare(key, curr.key);
            if (c < 0) {
                curr = curr.left;
            } else if (c > 0) {
                curr = curr.right;
            } else {
                return curr;
            }
        }
        return curr;
    }

    private boolean desc(K k1, K k2) {
        return comparator.compare(k1, k2) <= 0;
    }

    private boolean absc(K k1, K k2) {
        return comparator.compare(k1, k2) > 0;
    }

    private boolean eq(K k1, K k2) {
        return comparator.compare(k1, k2) == 0;
    }

    private Node<K, V> entry(K key, V value) {
        Node<K, V> e = new Node<K, V>();
        e.key = key;
        e.value = value;
        return e;
    }

    public Map<K,V> printTree(){
        Map<K,V> arr = new HashMap<K, V>();
        runTree(root,arr);
        return arr;
    }

    private void runTree(Node<K, V> curr,Map<K,V> arr){
        if(curr!=null){
            runTree(curr.left,arr);
            arr.put(curr.getKey(),curr.getValue());
            runTree(curr.right,arr);
        }
    }
}