package com.khanas;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class EnumView {
    enum MenuOptions implements Printable {
        ADD {
            public void print() {
                logger.info("Enter index of binary tree: ");
                int index = sc.nextInt();
                logger.info("Enter string for binary tree: ");
                sc.nextLine();
                String str = sc.nextLine();
                tree.put(index, str);
            }
        }, GET {
            public void print() {
                logger.info("Enter index of binary tree: ");
                int index = sc.nextInt();
                sc.nextLine();
                logger.info(tree.get(index));
            }
        }, REMOVE {
            public void print() {
                logger.info("Enter index of binary tree: ");
                int index = sc.nextInt();
                sc.nextLine();
                tree.remove(index);
            }
        }, PRINT {
            public void print() {
                Map<Integer, String> arr = tree.printTree();
                logger.info(arr);
            }
        };
    }

    static Logger logger = LogManager.getLogger();
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner sc = new Scanner(System.in);
    static BinaryTreeMap<Integer, String> tree;
    public EnumView() {
        tree = new BinaryTreeMap<Integer, String>(new Comparator<Integer>() {
            public int compare(Integer o1, Integer o2) {
                return o1.compareTo(o2);
            }
        });
        menu = new LinkedHashMap<String, String>();
        menu.put("1", "  1 - add to tree");
        menu.put("2", "  2 - get from tree");
        menu.put("3", "  3 - remove from tree");
        menu.put("4", "  4 - print tree");
        menu.put("5", "  5 - exit");

        show();
    }



    private void outputMenu() {
        logger.info("MENU:");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }

    public void show() {
        int keyMenu;
        do {
            outputMenu();
            logger.info("Please, select menu point.");
            keyMenu = sc.nextInt();
            try {
                MenuOptions.values()[keyMenu-1].print();
            } catch (Exception e) {
            }
        } while (keyMenu!=5);
    }
}

